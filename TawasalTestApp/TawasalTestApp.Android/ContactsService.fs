﻿namespace TawasalTestApp.Android

open System.Collections.Generic
open System.Threading.Tasks

open Android.App
open Android.Database
open Android.Provider

open Xamarin.Forms;

open TawasalTestApp.Models
open TawasalTestApp.Services

module ContactsService = 

    let getPhoneNumber (idQ : string[]) : string =
        let uri = ContactsContract.CommonDataKinds.Phone.ContentUri.BuildUpon().AppendQueryParameter(ContactsContract.RemoveDuplicateEntries, "1").Build()
        let query = sprintf "%s=?" ContactsContract.CommonDataKinds.Phone.InterfaceConsts.ContactId
        let cursor = Application.Context.ContentResolver.Query(uri, null, query, idQ, null)

        if cursor.MoveToFirst() then
            cursor.GetString(cursor.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.Number))
        else
            null
    

    let getName (idQ : string) : string * string = 
        let whereNameParams = [| ContactsContract.CommonDataKinds.StructuredName.ContentItemType |]
        let whereName = sprintf "%s = ? AND %s = %s" ContactsContract.Data.InterfaceConsts.Mimetype ContactsContract.CommonDataKinds.StructuredName.InterfaceConsts.ContactId idQ
        use cursor = Application.Context.ContentResolver.Query(ContactsContract.Data.ContentUri, null, whereName, whereNameParams, null)

        if cursor = null then (null, null)
        elif cursor.MoveToFirst() <> true then (null, null)
        else (
            cursor.GetString(cursor.GetColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GivenName)),
            cursor.GetString(cursor.GetColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FamilyName)))


    let getContact (cursor : ICursor, idKey : string) : Contact =
        let idQ = [| cursor.GetString(cursor.GetColumnIndex(idKey)) |]
        let name = getName(idQ.[0])
        let phoneNumber = getPhoneNumber(idQ)
        let contact = { FirstName = fst(name); LastName = snd(name); PhoneNumber = phoneNumber }
        contact


    let getEnumerableOfContacts (cursor : ICursor) : IEnumerable<Contact> =
        if cursor = null then null
        else
            let result = 
                [ if cursor.MoveToFirst() then
                    yield getContact(cursor, ContactsContract.Contacts.InterfaceConsts.Id)
                    
                    while cursor.MoveToNext() do
                        yield getContact(cursor, ContactsContract.Contacts.InterfaceConsts.Id) ]

            cursor.Close();
            result :> IEnumerable<Contact>


    type ContactsService() = 
        interface IContactsService with
            member this.GetAllContacts() =
                let cursor = Application.Context.ContentResolver.Query(ContactsContract.Contacts.ContentUri, null, null, null, null)
                Task.FromResult(getEnumerableOfContacts(cursor))


    [<assembly: Dependency(typeof<ContactsService>)>]
    do ()