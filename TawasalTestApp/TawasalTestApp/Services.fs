﻿namespace TawasalTestApp

open System.Collections.Generic
open System.Threading.Tasks

open TawasalTestApp.Models

module Services =
    type IContactsService =
        abstract GetAllContacts: unit -> Task<IEnumerable<Contact>>

