﻿namespace TawasalTestApp

[<RequireQualifiedAccess>]
module Strings =

    let SyncPage_Title = "Ваши контакты"

    let SyncPage_SyncButtonText = "Синхронизировать контакты"

    let SyncPage_Contacts_Empty = "Список контактов пуст.\nНажмите кнопку \"" + SyncPage_SyncButtonText + "\""

    let SyncPage_Status_NoSyncEver = "Синхронизация не производилась"
    let SyncPage_Status_SyncInProcess = "Синхронизация начата"
    let SyncPage_Status_SyncCompletedWithSuccess = "Синхронизация завершена"
    let SyncPage_Status_SyncCompletedWithError = "Синхронизация завершилась с ошибкой"
