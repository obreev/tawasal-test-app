﻿namespace TawasalTestApp

module Models =
    type Contact = {
        FirstName : string
        LastName : string
        PhoneNumber: string
    }
