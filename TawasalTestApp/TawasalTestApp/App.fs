﻿// Copyright Fabulous contributors. See LICENSE.md for license.
namespace TawasalTestApp

open Fabulous
open Fabulous.XamarinForms
open Xamarin.Forms
open Xamarin.Essentials

open TawasalTestApp.Models
open TawasalTestApp.Services
open TawasalTestApp.ContactsRepository

module App = 

    type Model = { 
        Status: string
        SyncInProcess: bool
        Contacts: Contact list }


    type Msg =
        | Void
        | NoSyncEver of Contact list
        | SyncInProcess
        | SyncCompletedWithSuccess of Contact list
        | SyncCompletedWithError


    let requestContactsPermissionAsync() = async {
        try
            let! status =
                Permissions.CheckStatusAsync<Permissions.ContactsRead>()
                |> Async.AwaitTask
            return status = PermissionStatus.Granted
        with _ ->
            return false
    }


    let checkContactsPermissionsAsync() = async {
        try
            let! status = requestContactsPermissionAsync()

            if status then
                return true
            else
                let! statusAfterRequest =
                    Permissions.RequestAsync<Permissions.ContactsRead>()
                    |> Async.AwaitTask
                return statusAfterRequest = PermissionStatus.Granted
        with _ ->
            return false
    }
    

    let initCompletion = 
        async {
            try
                do! Async.SwitchToThreadPool()
                let! contacts = loadAllContacts()
                return NoSyncEver contacts
            with _ ->
                return NoSyncEver List.Empty
        } |> Cmd.ofAsyncMsg


    let handleSyncInProcess =
        async {
            let! checkContactsPermissions = checkContactsPermissionsAsync()
            if checkContactsPermissions = false then
                return SyncCompletedWithError
            else
                let contactsService = Xamarin.Forms.DependencyService.Get<IContactsService>()

                do! Async.SwitchToThreadPool()

                let! contacts = contactsService.GetAllContacts() |> Async.AwaitTask
                if contacts = null then
                    return SyncCompletedWithError
                else
                    return SyncCompletedWithSuccess (contacts |> Seq.cast |> Seq.toList)
        } |> Cmd.ofAsyncMsg


    let handleSyncCompletedWithSuccess(contacts) =
        async {
            do! Async.SwitchToThreadPool()
            do! deleteAllContacts()
            do! insertAllContacts(contacts)
            return Void
        } |> Cmd.ofAsyncMsg


    let initModel = { Status = Strings.SyncPage_Status_NoSyncEver; SyncInProcess = true; Contacts = List.Empty }


    let init() = initModel, initCompletion


    let update (msg: Msg) (model: Model) =
        match msg with
        | NoSyncEver contacts ->
            { model with Status = Strings.SyncPage_Status_NoSyncEver; SyncInProcess = false; Contacts = contacts |> List.sortBy (fun c -> c.FirstName + c.LastName) },
            Cmd.none
        | SyncInProcess ->
            { model with Status = Strings.SyncPage_Status_SyncInProcess; SyncInProcess = true },
            handleSyncInProcess
        | SyncCompletedWithSuccess contacts ->
            { model with Status = Strings.SyncPage_Status_SyncCompletedWithSuccess; SyncInProcess = false; Contacts = contacts |> List.sortBy (fun c -> c.FirstName + c.LastName) }, 
            handleSyncCompletedWithSuccess(contacts)
        | Void -> 
            { model with Status = Strings.SyncPage_Status_SyncCompletedWithSuccess; SyncInProcess = false },
            Cmd.none
        | SyncCompletedWithError ->
            { model with Status = Strings.SyncPage_Status_SyncCompletedWithError; SyncInProcess = false },
            Cmd.none


    let contactItemView(contact : Contact) : ViewElement =
        View.StackLayout(
            orientation=StackOrientation.Vertical,
            margin = Thickness(0., 4., 0., 4.),
            spacing = 4.,
            children = [
                View.Label(
                    text = sprintf "%s %s" contact.FirstName contact.LastName,
                    textColor = Color.Black,
                    fontSize = FontSize.Value.Size(16.),
                    margin = Thickness(16., 0., 16., 0.)
                )
                View.Label(
                    text = contact.PhoneNumber,
                    margin = Thickness(16., 0., 16., 0.)
                )
                View.BoxView(
                    height = 1.,
                    backgroundColor = Color.LightGray
                )
            ]
        )


    let view (model: Model) dispatch =
        View.ContentPage(
            content = View.Grid(
                rowdefs = [ Star; Auto; Auto ],
                margin = Thickness(0., 16., 0., 16.),
                children=[
                    View.CollectionView(
                        emptyView = View.Grid(
                            children = [
                                View.Label(
                                    text = Strings.SyncPage_Contacts_Empty,
                                    isVisible = not model.SyncInProcess,
                                    horizontalOptions = LayoutOptions.Center,
                                    verticalOptions = LayoutOptions.Center,
                                    horizontalTextAlignment = TextAlignment.Center
                                )
                            ]
                        ),
                        items = [ for contact in model.Contacts do
                                    yield contactItemView contact ]
                    )

                    View.Grid(
                        isVisible = model.SyncInProcess,
                        children = [
                            View.ActivityIndicator(
                                horizontalOptions = LayoutOptions.Center,
                                verticalOptions = LayoutOptions.Center,
                                isRunning = true,
                                color = Color.Black
                            )
                        ]
                    )

                    View.Label(
                        text = sprintf "%s" model.Status)
                        .Row(1)
                        .HorizontalOptions(LayoutOptions.Center)

                    View.Button(
                        text = Strings.SyncPage_SyncButtonText,
                        command = (fun () -> dispatch SyncInProcess))
                        .HorizontalOptions(LayoutOptions.Center)
                        .Row(2)
                ]
            )
        )


    let program =
        XamarinFormsProgram.mkProgram init update view
#if DEBUG
        |> Program.withConsoleTrace
#endif


type App () as app = 
    inherit Application ()

    let runner = 
        App.program
        |> XamarinFormsProgram.run app
