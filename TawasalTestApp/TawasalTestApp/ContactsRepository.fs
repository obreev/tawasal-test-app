﻿namespace TawasalTestApp

open System.IO

open Xamarin.Essentials

open TawasalTestApp.Models
open SQLite

module ContactsRepository =

    let dbName = "SqliteDatabase.db3"
    let dbPath = Path.Combine(FileSystem.AppDataDirectory, dbName)

    type ContactObject() =
        [<PrimaryKey; AutoIncrement>]
        member val Id = 0 with get, set
        member val FirstName = "" with get, set
        member val LastName = "" with get, set
        member val PhoneNumber = "" with get, set

    let convertToObject (item: Contact) =
        let obj = ContactObject()
        obj.FirstName <- item.FirstName
        obj.LastName <- item.LastName
        obj.PhoneNumber <- item.PhoneNumber
        obj

    let convertToModel (obj: ContactObject) : Contact =
        { FirstName = obj.FirstName
          LastName = obj.LastName
          PhoneNumber = obj.PhoneNumber }

    let connect(dbPath) = async {
        let db = SQLiteAsyncConnection(SQLiteConnectionString dbPath)
        do! db.CreateTableAsync<ContactObject>() |> Async.AwaitTask |> Async.Ignore
        return db
    }

    let insertAllContacts(contacts) = async {
        let! database = connect dbPath
        let contactsObjects = contacts |> Seq.toList |> List.map convertToObject 
        do! database.InsertAllAsync(contactsObjects) |> Async.AwaitTask |> Async.Ignore
    }

    let loadAllContacts() = async {
        let! database = connect dbPath
        let! objs = database.Table<ContactObject>().ToListAsync() |> Async.AwaitTask
        return objs |> Seq.toList |> List.map convertToModel
    }

    let deleteAllContacts() = async {
        let! database = connect dbPath
        do! database.DeleteAllAsync<ContactObject>() |> Async.AwaitTask |> Async.Ignore
    }


