﻿namespace TawasalTestApp.iOS

open System.Collections.Generic
open System.Threading.Tasks

open Contacts

open Xamarin.Forms

open TawasalTestApp.Models
open TawasalTestApp.Services

module ContactsService =

    let getContact(contact : CNContact) : Contact =
        let phones = contact.PhoneNumbers
        let phones2 = [
            for phone in phones do
                yield phone.Value.StringValue ]
        let firstPhone = phones2 |> List.head
        let contact = { FirstName = contact.GivenName; LastName = contact.FamilyName; PhoneNumber = firstPhone }
        contact


    let keys = [| CNContactKey.GivenName; CNContactKey.FamilyName; CNContactKey.PhoneNumbers |]


    let getEnumerableOfContacts() : IEnumerable<Contact> =
        let store = new CNContactStore()
        let containers = fst(store.GetContainers(null))
        match containers with
            | null -> List.Empty :> IEnumerable<Contact>
            | _ -> [
                for container in containers do
                    use pred = CNContact.GetPredicateForContactsInContainer(container.Identifier)
                    let contacts = fst(store.GetUnifiedContacts(pred, keys))
                    let contacts2 = [
                        for contact in contacts do
                            yield getContact(contact)]
                    yield! contacts2] :> IEnumerable<Contact>


    type ContactsService() = 
        interface IContactsService with
            member this.GetAllContacts() =
                Task.FromResult(getEnumerableOfContacts())

    [<assembly: Dependency(typeof<ContactsService>)>]
    do ()
